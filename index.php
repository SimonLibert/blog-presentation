<!DOCTYPE html>
<html lang="en">

<head>
  <title>Changement de cap!</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="blogpresentation.css" />
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light static-top mb-5 shadow">
    <div class="container">
      <a class="navbar-brand" href="#">Simon Libert</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="photos/cvsimonlibert.zip" download="CV Simon Libert.zip">Télécharger mon CV</a>
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#mon parcours professionnel">A propos de moi</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#formulaire-contact">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
<div class="container">
<div class="row"> 
  <div class="col-sm-12">
    <div class="video">
      <div class="embed-responsive embed-responsive-21by9">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/kZIfjsZKevs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>

  <!-- contenu de page -->
  <div id="mon parcours professionnel"></div>
		<div class="row">
			<div class="col-md-6">
				<div class="card">
					<div class="card-body">
						<h1 class="font-weight-light">Mon parcours professionnel</h1>
							<p class="lead">Titulaire d'un baccalauréat littéraire et musicien passionné, je choisis de poursuivre mes
							études post-bac à la faculté de Lille 3 ou je rentre en cursus "Etudes artistiques et culturelles".
							Etudes que j'arrête afin de devenir autonome financièrement; parallèlement je me produis en concerts
							régulièrement en France et à l'étranger au sein de divers formations musicales.
							Puis je décide d'arrêter les "petits boulots" afin de me consacrer à ma passion qui est la musique et rentre
							au Conservatoire de musique de Boulogne sur mer
							où j'obtiens mon diplôme de 3ème cycle professionnel (DNOP). En même temps, le nombre de concerts effectués
							me
							permet de devenir musicien intermittent du spectacle.
							Je suis également professeur de guitare et de basse au sein d'une école de musique.</p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-body">
						<h1 class="font-weight-light">Pourquoi je veux être développeur web</h1>
						<p class="lead">En mars 2020, le covid pointe le bout de son nez et voyant tous nos concerts s'annuler, un ami musicien me propose
						de m'apprendre les rudiments du langage HTML et CSS, grâce à l'éditeur de texte notepade ++. Je commence par faire des petites manips, mettre des mots dans des balises, 
						qui vont tantôt modifier les couleurs d'un texte, d'un titre, tantôt appliquer un fond d'écran, etc... Pas grand chose, mais quelle victoire à chaque fois!! Puis, peu à peu je me suis 
						mis à faire des petites pages, comme un faux blog, une lettre, un cv, et à suivre le parcours "Open Classrooms" en HTML et CSS. On y était! Il avait su 
						m'apporter ce petit "truc", me donner envie d'aller plus loin, de pouvoir créer, manipuler et apprendre un nouveau langage.</br>
						Aujourd'hui la situation n'a pas beaucoup changé, le covid est toujours bien installé, et malgré les quelques concerts en été 2020, mon avenir en tant que musicien professionnel
						reste plus qu'incertain.</br>
						En parallèle, j'ai continué mon apprentissage de manière auto-didacte (en ayant recours parfois à l'aide des copains ^^) des langages HTML et CSS en y intégrant BOOTSTRAP, 
						j'ai travaillé sur la création de sites WEB de mes deux groupes de musique tout en réfléchissant à mon avenir.
						C'est décidé, je vais franchir le cap et candidater à une formation de développeur WEB.
						C'est sûr, la musique va continuer d'exister chez moi car il m'est impossible de vivre sans, mais plus en tant que ressource principale, 
						métier devenu trop instable pour un père de trois enfants.</br> Mes ambitions? &Ecirctre pris au sein de la formation SIMPLON, m'enrichir d'expériences professionnelles dans divers domaines pour
						certainement entrer dans la voie d'une spécialisation qui me correspondra au mieux!
						</p>
					</div>
				</div>
			</div>
		</div>
  

<footer>
  <!-- <?php include('form.php'); ?> -->
  <div id="formulaire-contact">
	  <div class="row">
			<div class="col-sm-12">
				  <form action="form.php" method="post">
						<div>      
						  <label for="mail">E-mail :</label>
							<input type="Email" id="mail" name="mail" />
						</div>
						<div>
						  <label for="msg">Message :</label>
						  <textarea id="msg" name="msg"></textarea>
						</div>
						<div>
						  <input type="submit" value="Envoyer">
						</div>
				  </form>
			</div>
		</div>
  </div>
</footer>
</div>
</body>

</html>